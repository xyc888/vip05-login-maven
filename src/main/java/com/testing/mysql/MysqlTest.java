package com.testing.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;


public class MysqlTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String url="jdbc:mysql://localhost:3306/chenhui?useSSL=true&serverTimezone=GMT&useUnicode=true&autoReconnect=true&characterEncoding=utf-8";
		//String url="jdbc:mysql://localhost:3306/chenhui?characterEncoding=utf8&useSSL=false&serverTimezone=UTC&rewriteBatchedStatements=true"; 
		String user="root";
		String password="root";
		
		try {
			//加载驱动
			Class.forName("com.mysql.cj.jdbc.Driver");
			//建立连接
			Connection mysqlConnector=DriverManager.getConnection(url, user, password);
			//建立查询
			Statement myst=mysqlConnector.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE);
			//拼接sql语句
			String inputUser="Roy";
			String inputPwd="123456";
			String sql="SELECT * FROM `userinfo` where username='"+inputUser+"' and `password`='"+inputPwd+"';";
			ResultSet rs= myst.executeQuery(sql);
			rs.last(); 
			int rowCount = rs.getRow(); //获得ResultSet的总行数
			if(rowCount>0){
				System.out.println("user is : "+rs.getString("username"));
				System.out.println("password is:"+ rs.getString("password"));
				System.out.println("登录成功");
			}else{
				System.out.println("登录失败");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
